Definiciones
==========

###Ciclo de desarrollo ágil
Un ciclo de desarrollo ágil se compone de análisis y levantamiento de requerimientos, diseño y modelamiento de procesos, desarrollo, integración y pruebas.

###Pasos para desarrollar una aplicación web

Hablar con el cliente para presentar las propuestas y definir el alcance del proyecto, después se debe planificar los objetivos primarios y secundarios, con el fin de diseñar y modelar los principales aspectos de la aplicación, hasta la fase de desarrollo donde dentro de las iteraciones la aplicación irá tomando forma y una vez concluida se definirá el lanzamiento, ajustes y mantenimiento.


Solución conceptual
================

###Servicio web

Para proteger una aplicación de falsificaciones de solicitudes entre sitios usando el CSRF Django tiene dentro de sus middleware el CsrfViewMiddleware el cual protege las vistas para que requiera el token ante las request, para incluir este token en los formularios solo se debe agregar el {{csrf_token}} en el cuerpo del form y al realizar la petición se hará la verificación, ahora en caso de peticiones asíncronas existe un método de JS proporcionado por la documentación de django llamado getCookie('csrftoken'), pero este tiene un problema y es que no siempre se pueden acceder a las cookies, una forma de lidiar con este problema es creando un form y agregando el {{csrf_token}} al cuerpo como en las peticiones síncronas y este form se le envía a un FormData de JS el cual contiene el token.


###Aplicación de video

Los formatos compatibles para los principales navegadores del mercado son mp4, ogg y webm, siendo este último el mejor por calidad y compresión en video.

Como los usuarios pueden subir cualquier formato y calidad de video, para no afectar la experiencia de usuario la conversión no se puede llevar a cabo en el cliente debido a que esta podría tomar mucho tiempo o consumir muchos recursos, por ende la conversión se debe realizar en el servidor, cuando el archivo se suba, se iniciara un proceso de conversión usando librerías de apoyo como python-video-converter que usa FFmpeg framework, mientras el archivo se convierte no estará disponible y cuando el proceso termine se notificará al usuario de su disponibilidad.


Solución practica
===============

- Para correr el proyecto por primera vez

#### En consola
    pip install -r requirements.txt
	python manage.py makemigrations
	python manage.py migrate
	python manage.py runserver
