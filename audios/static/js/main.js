'use strict';

// Put variables in global scope to make them available to the browser console.
const audio = document.querySelector('audio');

let mediaRecorder;
let recordedBlobs;
let file;
const recordedAudio = document.querySelector('audio#recorded');

const constraints = window.constraints = {
  audio: true,
  video: false
};

function handleSuccess(stream) {
    const audioTracks = stream.getAudioTracks();
    stream.oninactive = function() {
    console.log('Stream ended');
    };
    window.stream = stream;
}

function handleDataAvailable(event) {
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function startRecording() {
    recordedBlobs = [];
    let options = {mimeType: 'audio/webm'};
    mediaRecorder = new MediaRecorder(window.stream, options);
    recordButton.textContent = 'Stop Recording';
    mediaRecorder.onstop = (event) => {
        console.log('Recorder stopped: ', event);
    };

    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.start(10); // collect 10ms of data
}

function stopRecording() {
  mediaRecorder.stop();
  const superBuffer = new Blob(recordedBlobs, {type: 'audio/webm;codecs=opus'});
  file = new File([superBuffer], `${Math.random().toString(36).substr(2, 10)}.webm`);
}

const playButton = document.querySelector('button#play');
playButton.addEventListener('click', () => {
    recordedAudio.src = null;
    recordedAudio.srcObject = null;
    recordedAudio.src = window.URL.createObjectURL(file);
    recordedAudio.controls = true;
    recordedAudio.play();
});

const recordButton = document.querySelector('button#record');

recordButton.addEventListener('click', () => {
  if (recordButton.textContent === 'Start Recording') {
    startRecording();
  } else {
    stopRecording();
    recordButton.textContent = 'Start Recording';
    downloadButton.disabled = false;
  }
});

const downloadButton = document.querySelector('button#download');
downloadButton.addEventListener('click', () => {
  const url = window.URL.createObjectURL(file);
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'test.webm';
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
});

const saveButton = document.querySelector('button#save');
saveButton.addEventListener('click', () => {
    const form = $('#form-audio');
    var formData = new FormData(form[0]);
    formData.append("file", file);

    fetch(form.attr('action'), {
      method: form.attr('method'),
      credentials: "same-origin",
      body: formData
    })
    .then(response => response.text())
    .catch(error => console.error('Error:', error))
    .then(response => $("#audios-list").prepend(response));
});

const audioslist = $('#audios-list');
audioslist.on('click', '.delete', function () {

    $(this).parent().remove();
    const id = $(this)[0].id;

    const form = $('#form-delete');
    const formData = new FormData(form[0]);

    fetch(`/delete/${id}/`, {
      method: 'post',
      credentials: "same-origin",
      body: formData
    })
    .then(response => console.log('Response', response))
    .catch(error => console.error('Error:', error))
});

function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}

navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
