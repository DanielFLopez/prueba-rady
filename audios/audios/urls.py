from django.urls import path

from .views import AudioCreateView, AudioDeleteView

app_name = 'audios'
urlpatterns = [
    path('', AudioCreateView.as_view(), name='create'),
    path('delete/<int:pk>/', AudioDeleteView.as_view(), name='delete'),
]
