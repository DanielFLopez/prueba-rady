from django import forms

from audios.models import Audio


class AudioForm(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ['file']
