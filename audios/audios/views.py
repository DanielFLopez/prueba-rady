from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView

from audios.forms import AudioForm
from audios.models import Audio


class AudioCreateView(CreateView):
    """
    Vista para creacion de audios
    """
    model = Audio
    form_class = AudioForm
    success_url = reverse_lazy('audios:create')

    def get_context_data(self, **kwargs):
        context = super(AudioCreateView, self).get_context_data(**kwargs)
        context['audios'] = Audio.objects.all()
        return context

    def form_valid(self, form):
        super(AudioCreateView, self).form_valid(form)
        return render(self.request, 'audios/_audiocomponent.html', {'object': self.object})


class AudioDeleteView(DeleteView):
    """
    Vista para borrar audios
    """
    model = Audio
    success_url = reverse_lazy('audios:create')

    def delete(self, request, *args, **kwargs):
        super(AudioDeleteView, self).delete(request, *args, **kwargs)
        return JsonResponse({'status': 200})
