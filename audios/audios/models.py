from django.db import models


class Audio(models.Model):
    file = models.FileField()
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.file.name)
